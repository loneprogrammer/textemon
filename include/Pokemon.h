#ifndef POKEMON_H
#define POKEMON_H
#include <string>
#include "Attack.h"
#include <vector>
class Pokemon
{
    public:
  //      Pokemon(unsigned int level, std::string name);
        Pokemon(std::string name, unsigned int attack1id, unsigned int attack2id, unsigned int attack3id,unsigned int attack4id, unsigned int level, unsigned int cexp,unsigned int rexp, std::vector<Attack> allmoves);
        std::string findPokemonInFile(std::string);
        Attack moves[4];
        Attack findAndAssignAttacks(unsigned int);
        std::vector<Attack> allmoves;
        void printPokemonInfo();
        void recalculateStats(); //uses the stats formula to calculate more realistic stats, now that the level has been accessed
        std::string name;
        unsigned int hp;
        unsigned int attack;
        unsigned int defense;
        unsigned int specialAttack;
        unsigned int specialDefense;
        unsigned int speed;
        unsigned int attack_1_ID;
        unsigned int attack_2_ID;
        unsigned int attack_3_ID;
        unsigned int attack_4_ID;
        unsigned int level;
        unsigned int currentEXP;
        unsigned int requiredEXP;
        unsigned int getPokemonBaseStat(std::string stat, std::string pokemonName);
};

#endif // POKEMON_H
