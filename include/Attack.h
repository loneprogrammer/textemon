#ifndef ATTACK_H
#define ATTACK_H
#include <string>

class Attack //To be used with a loop that iterates through a text file containing move data
{
    public:
        Attack(unsigned int lineInDataFile);
        Attack();
        void printMoveProperties();
        unsigned int ID;
        unsigned int power;
        unsigned int accuracy;
        std::string name;
        std::string type;
};

#endif // ATTACK_H
