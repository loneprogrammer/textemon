#ifndef BATTLE_H
#define BATTLE_H
#include <vector>
#include <string>
#include "Pokemon.h"
#include <fstream>
#include <iostream>
#include <cmath>
#include <stdlib.h>
class Battle
{
    public:
        typedef unsigned int uint;
        Battle(std::vector<Pokemon>);
        std::vector<Pokemon> playerParty;
        unsigned int countLinesInTextFile(std::string);
        std::vector<Pokemon> opponentParty;
        void createOpponentParty();
        void processRAWListOfPokemon(std::vector<std::string>);
        void getANDStoreAllMoves();
        std::vector<Attack> allmoves;
        std::vector<std::string> returnRAWListOfPokemonandStats();
};

#endif // BATTLE_H
