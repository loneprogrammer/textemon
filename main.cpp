#include <iostream>
#include <fstream>
#include <string>
#include "stdio.h"
#include <vector>
#include <stdlib.h>
#include "Pokemon.h"
#include "Attack.h"
#include "Battle.h"
void getNumberOfPARTYPokemonandPopulatePartyVector();
void getAllMoves(); //Loads all moves into an attack VECTOr
void playIntro(); // Displays a simple intro
void displayMenu(); //Shows menu options
void createPlayerFile(); //Creates a new player file
void readPokemonStatsInPlayerFile(unsigned int);
void printPartyInformation();


std::vector<Pokemon> party; // A global vector representing the player's party
std::vector<Attack> allmoves; // A global vector that holds ALL POKEMON MOVES

using namespace std;

int main()
{
    try
    {
      getAllMoves();
    }
    catch(...)
    {
        std::cout << "Couldnt populate moves vector. Check allmoves.txt" << std::endl;
    }
    try
    {
        getNumberOfPARTYPokemonandPopulatePartyVector();
    }
    catch(...)
    {
        std::cout << "Couldnt get party pokemon information. Check party.txt" << std::endl;
    }

playIntro();
displayMenu();
printPartyInformation();
/* Playing with pointers - unnecessary stuff
std::string* addressofpartyvector = &party[0].name;
std::cout << "The address of the first pokemon in your party is: " << addressofpartyvector << std::endl;
std::cout << "The name of the pokemon at the memory location " << addressofpartyvector << " is " << *addressofpartyvector << std::endl;
*/
}
void playIntro()
{
    std::cout << "Textemon by Luke Chen Shui" << std::endl;
}
void displayMenu()
{   retryChoices:
    std::cout << "Please choose one of the following options by entering its corresponding letter:" << std::endl;
    std::cout << "a. Create new file" << std::endl;
    std::cout << "b. Play the game." << std::endl;
    char choice = std::cin.get();
    if (choice == 'a')
    {
        //Create new player file
        createPlayerFile();

    }
    else if (choice == 'b')
    {
        Battle newBattle(party);
        //Play the game

    }
    else
    {
        std::cout<< "You have entered an invalid choice. Please try again:" << std::endl;
        goto retryChoices;
    }
}
void createPlayerFile()
{
        if ( std::cin.peek() == '\n' ) //Prevents getline() from being skipped
        {
            std::cin.ignore();
        }
        remove("Data/player.txt"); //Completely deletes the previous file, if any exists.
        std::ofstream playerFileCreator;
        playerFileCreator.open("Data/player.txt");
        std::cout << "Please enter your player's name: ";
        std::string playerName="";
        std::getline(std::cin,playerName);
        retryChoosePokemon: //Comes here if the player entered an invalid option
        std::cout << "Please choose one of the following pokemon by typing in its associated letter:" << std::endl;
        std::cout << "a. Bulbasaur" << std::endl;
        std::cout << "b. Charmander" << std::endl;
        std::cout << "c. Squirtle" << std::endl;

        std::cout << "d. Pikachu" << std::endl;

        std::cout << "e. Chikorita" << std::endl;
        std::cout << "f. Cyndaquil" << std::endl;
        std::cout << "g. Totodile" << std::endl;

        std::cout << "h. Treecko" << std::endl;
        std::cout << "i. Torchic" << std::endl;
        std::cout << "j. Mudkip" << std::endl;

        std::cout << "k. Turtwig" << std::endl;
        std::cout << "l. Chimchar" << std::endl;
        std::cout << "m. Piplup" << std::endl;

        std::cout << "n. Snivy" << std::endl;
        std::cout << "o. Tepig" << std::endl;
        std::cout << "p. Oshawott" << std::endl;

        std::cout << "b. Charmander" << std::endl;
        std::cout << "c. Squirtle" << std::endl;
        char choice = std::cin.get();
        std::string pokemonChoice="";
        if (choice == 'a')
        {
            pokemonChoice = "Bulbasaur";
            //Give him Bulbasaur
        }
        else if (choice == 'b')
        {
            pokemonChoice = "Charmander";
            //Give him Charmander
        }
        else if (choice == 'c')
        {
            pokemonChoice = "Squirtle";
            //Give him Squirtle
        }
        else if (choice == 'd')
        {
            pokemonChoice = "Pikachu";
            //Give him Squirtle
        }
        else if (choice == 'e')
        {
            pokemonChoice = "Chikorita";
            //Give him Bulbasaur
        }
        else if (choice == 'f')
        {
            pokemonChoice = "Cyndaquil";
            //Give him Charmander
        }
        else if (choice == 'g')
        {
            pokemonChoice = "Totodile";
            //Give him Squirtle
        }
        else if (choice == 'h')
        {
            pokemonChoice = "Treecko";
            //Give him Bulbasaur
        }
        else if (choice == 'i')
        {
            pokemonChoice = "Torchic";
            //Give him Charmander
        }
        else if (choice == 'j')
        {
            pokemonChoice = "Mudkip";
            //Give him Squirtle
        }
        else if (choice == 'k')
        {
            pokemonChoice = "Turtwig";
            //Give him Bulbasaur
        }
        else if (choice == 'l')
        {
            pokemonChoice = "Chimchar";
            //Give him Charmander
        }
        else if (choice == 'm')
        {
            pokemonChoice = "Piplup";
            //Give him Squirtle
        }
        else if (choice == 'n')
        {
            pokemonChoice = "Snivy";
            //Give him Bulbasaur
        }
        else if (choice == 'o')
        {
            pokemonChoice = "Tepig";
            //Give him Charmander
        }
        else if (choice == 'p')
        {
            pokemonChoice = "Oshawott";
            //Give him Squirtle
        }
        else
        {
            goto retryChoosePokemon;
            //The user entered an invalid choice; Give him another chance
        }
        playerFileCreator << playerName << std::endl;
        playerFileCreator << '1' << std::endl;
        playerFileCreator << pokemonChoice <<" 0 1 0 1 5 0 1000 " << std::endl;
        /*att = attack
        def = defense
        spa = special attack
        spd = special defense
        spe = speed
        lvl = level
        at1 - 4  = attacks 1 to 4 (attacks are identified by their id#
        cxp = current experience points
        nxp = experience to get to the next level
        */
}
void readPokemonStatsInPlayerFile(unsigned int lineNumber) //Skips to this line number to read the pokemon's info
{
    std::ifstream readPlayerFile("Data/player.txt");
    std::string pokemonInfoLine = "";
    if (readPlayerFile.is_open()== true)
    {
        unsigned int x = 0;
    while ( getline (readPlayerFile,pokemonInfoLine) )
    {
      if (x == lineNumber) //Stops when the stream reaches the desired line
      {
          break;
      }
      x++;
    }
    }
  //  std::cout<< pokemonInfoLine; //Prints the contents of the line
    std::vector<std::string> pokemonInfo;
    std::string temporaryString;
    for(unsigned int x = 0; x < pokemonInfoLine.length(); x++)
        // EXAMPLE: Bulbasaur att:10 def:10 spa:10 spd:10 spe:10 at1:1 at2:2 at3:3 at4:4 lvl:1 cxp:0 nxp:1000
    {
        if(pokemonInfoLine[x] != ' ') //Identifies each attribute via the space
        {
            temporaryString+=pokemonInfoLine[x];
        }
        else
        {
            pokemonInfo.push_back(temporaryString);
            temporaryString="";
        }
    }
    //Each attribute now has its own space in the vector
    std::cout << "Vector size: " <<pokemonInfo.size()<< std::endl;
    std::string pokemonName =pokemonInfo[0];
    unsigned int attack1ID = atoi(pokemonInfo[1].c_str());
    unsigned int attack2ID = atoi(pokemonInfo[2].c_str());
    unsigned int attack3ID = atoi(pokemonInfo[3].c_str());
    unsigned int attack4ID = atoi(pokemonInfo[4].c_str());
    unsigned int level = atoi(pokemonInfo[5].c_str());
    unsigned int currentExperience = atoi(pokemonInfo[6].c_str());
    unsigned int experienceNeeded = atoi(pokemonInfo[7].c_str());
    std::cout << "Initial attack 4 id: " << attack4ID << std::endl;
    //EXAMPLE: att:10 becomes 10
    Pokemon temporary(pokemonName, attack1ID,attack2ID,attack3ID,attack4ID,level, currentExperience, experienceNeeded, allmoves);
    temporary.printPokemonInfo();
party.push_back(temporary);
readPlayerFile.close();
//Create a temporary pokemon variable and places it in the global party vector

 //std::cout << pokemonName << " has an attack of " << attack <<" a defense of" << defense << " a special attack of " << specialAttack << "a special defense of " << specialDefense << "and a speed of " << speed << ". " << pokemonName << " knows moves: " << attack1ID << " " << attack2ID << " " << attack3ID << " " << attack4ID << " and is at level" << level << " and has " << currentExperience << "and requires " << experienceNeeded << "To advance to the next level." << std::endl;
}

void getAllMoves()
{
    int number_of_lines = 0;
    std::string line;
    std::ifstream myfile("Data/Pokemon/all moves.txt");

    while (std::getline(myfile, line))
        ++number_of_lines;
//        std::cout << "number of lines:" << number_of_lines << std::endl;
    for (unsigned int counter = 0; counter < number_of_lines;counter++)
    {
        Attack temporary(counter);
        allmoves.push_back(temporary);
  //      temporary.printMoveProperties();
    }
    if(allmoves.size() == 0)
    {
        throw "no moves";
    }
}
void printPartyInformation()
{
    for (unsigned int x = 0; x < party.size(); x++)
    {
        party[x].printPokemonInfo();
    }
}
void getNumberOfPARTYPokemonandPopulatePartyVector()
{
unsigned int numberOfPokemon = 0;
    std::ifstream scanPlayerFile("Data/player.txt");
    if(scanPlayerFile.is_open() == true)
    {
        std::string temporary = "";
        unsigned int x = 0;
        while (std::getline(scanPlayerFile, temporary))
        {
            if (x == 1)
            {
                numberOfPokemon =  atoi(temporary.c_str());
                break;
            }
            x++;
        }
    }
    //std::cout << "Number of pokemon in party : " << numberOfPokemon << std::endl;
    for (unsigned int counter = 0; counter < numberOfPokemon; counter ++)
    {
     unsigned int address =  2+counter;
     readPokemonStatsInPlayerFile(address); //Where the pokemon's information will be
    }
    if(party.size() == 0)
    {
        throw "empty party";
    }
}
/*
Attack searchAttackByID(unsigned int ID)
{

}
*/
