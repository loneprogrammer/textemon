#include "Attack.h"
#include <vector>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <iostream>
Attack::Attack(unsigned int lineInDataFile)
{
    ID = lineInDataFile;
    std::vector<std::string> moveInfo;
    std::ifstream readMoveFile("Data/Pokemon/all moves.txt");
    std::string attackInfoLine = "";
    if (readMoveFile.is_open()== true)
    {
        unsigned int x = 0;
    while ( getline (readMoveFile,attackInfoLine) )
    {
      if (x == lineInDataFile) //Stops when the stream reaches the desired line
      {
          break;
      }
      x++;
    }
    }
    std::string temporaryString;
    for(unsigned int x = 0; x < attackInfoLine.length(); x++)
    {
        if(attackInfoLine[x] != ':') //Identifies each attribute via the space
        {
            temporaryString+=attackInfoLine[x];
        }
        else
        {
          //  std::cout << temporaryString << std::endl;
            moveInfo.push_back(temporaryString);
            temporaryString="";
        }

    }
    name = moveInfo[0];
    type = moveInfo[1];
    power = atoi(moveInfo[2].c_str());
    accuracy = atoi(moveInfo[3].c_str());
}
void Attack::printMoveProperties()
{
    std::cout << name << " has a power of " << power << ", a type of " << type << " and an accuracy of " << accuracy << std::endl;
}
Attack::Attack()
{

}
