#include "Battle.h"
#include <time.h>

Battle::Battle(std::vector<Pokemon> vectorToUseToMakePlayerParty)
{
    getANDStoreAllMoves();
    playerParty = vectorToUseToMakePlayerParty;
    createOpponentParty();
}
void Battle::createOpponentParty()
{
    std::cout << "Opponent Information:" << std::endl;
 typedef unsigned int uint;
 for (uint x =0; x < playerParty.size();x++)
 {
      std::vector<std::string> allPokemon =  returnRAWListOfPokemonandStats();
      processRAWListOfPokemon(allPokemon);
 }
}
void Battle::processRAWListOfPokemon(std::vector<std::string> allPokemon) //Gets list of all pokemon and creates a random new pokemon
{
    srand(time(NULL)); //Randomizes the seed

 uint randomPokemonChooser = rand() % allPokemon.size() + 0; //Generates a random number used to choose a random pokemon
 //std::cout << "Random number: " << randomPokemonChooser << std::endl;
std::string chosenPokemonInformation = allPokemon[randomPokemonChooser];

std::vector<std::string> pokemonInfo;
std::string temporaryString = "";
    for(unsigned int x = 0; x < chosenPokemonInformation.length(); x++)
        // EXAMPLE: Bulbasaur att:10 def:10 spa:10 spd:10 spe:10 at1:1 at2:2 at3:3 at4:4 lvl:1 cxp:0 nxp:1000
    {
        if(chosenPokemonInformation[x] != ':') //Identifies each attribute via the space
        {
            temporaryString+=chosenPokemonInformation[x];
        }
        else
        {
            pokemonInfo.push_back(temporaryString);
            temporaryString="";
        }
    }

    std::string pokemonName = pokemonInfo[0];
    unsigned int hp = atoi(pokemonInfo[1].c_str());
    unsigned int attack = atoi(pokemonInfo[2].c_str());
    unsigned int defense = atoi(pokemonInfo[3].c_str());
    unsigned int specialAttack = atoi(pokemonInfo[4].c_str());
    unsigned int specialDefense = atoi(pokemonInfo[5].c_str());
    unsigned int speed = atoi(pokemonInfo[6].c_str());

    unsigned int randomInt = rand() % allmoves.size() + 0;
    unsigned int attack1ID = allmoves[randomInt].ID;

    randomInt = rand() % allmoves.size() + 0;
    unsigned int attack2ID = allmoves[randomInt].ID;

    randomInt = rand() % allmoves.size() + 0;
    unsigned int attack3ID = allmoves[randomInt].ID;

    randomInt = rand() % allmoves.size() + 0;
    unsigned int attack4ID = allmoves[randomInt].ID;

    randomInt = rand() % playerParty.size() + 0;
unsigned int    level = playerParty[randomInt].level;
    int currentexp = 0;
    int requiredexp = 0;
    Pokemon temporaryPokemon(pokemonName,attack1ID,attack2ID,attack3ID,attack4ID,level ,currentexp,requiredexp, allmoves);
    opponentParty.push_back(temporaryPokemon);
    temporaryPokemon.printPokemonInfo();
}
std::vector<std::string> Battle::returnRAWListOfPokemonandStats() //Returns a list of all Pokemon and their stats
{
 std::vector<std::string> pokemonInFile;
 std::ifstream readAllPokemonStatstxt;
 readAllPokemonStatstxt.open("Data/Pokemon/All Pokemon Stats.txt.txt");
 std::string lineHolder = "";
 if (readAllPokemonStatstxt.is_open()== true)
 {

 while (std::getline(readAllPokemonStatstxt, lineHolder))
 {
     pokemonInFile.push_back(lineHolder);
    // std::cout << lineHolder << std::endl;
 }
 }
 else
 {
     std::cout <<"Couldn't access pokemon data file." << std::endl;
 }
 return pokemonInFile;
}



void Battle::getANDStoreAllMoves()
{
    allmoves.clear();

    unsigned int number_of_lines = countLinesInTextFile("Data/Pokemon/all moves.txt");
//        std::cout << "number of lines:" << number_of_lines << std::endl;
    for (unsigned int counter = 0; counter < number_of_lines;counter++)
    {
        Attack temporary(counter);
        allmoves.push_back(temporary);
  //      temporary.printMoveProperties();
    }
}
unsigned int Battle::countLinesInTextFile(std::string fileName)
{
    int number_of_lines = 0;
    std::string line;
    std::ifstream myfile(fileName.c_str());

    while (std::getline(myfile, line))
    ++number_of_lines;
    myfile.close();
    return number_of_lines;
}
