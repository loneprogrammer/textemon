#include <iostream>
#include <fstream>
#include <string>
#include "stdio.h"
#include <vector>
#include <stdlib.h>
#include "Pokemon.h"
#include "Attack.h"
#include "Battle.h"
/*Pokemon::Pokemon(unsigned int level, std::string name)
{
    //ctor
}
*/
Pokemon::Pokemon(std::string _name, unsigned int _attack1id, unsigned int _attack2id, unsigned int _attack3id,unsigned int _attack4id, unsigned int _level, unsigned int _cexp,unsigned int _rexp, std::vector<Attack> all_moves)
{

    name = _name;
attack_1_ID =  _attack1id;
attack_2_ID = _attack2id;
attack_3_ID = _attack3id;
attack_4_ID = _attack4id;
level= _level;
currentEXP = _cexp;
requiredEXP = _rexp;
recalculateStats();
allmoves = all_moves;
moves[0] = findAndAssignAttacks(attack_1_ID);
moves[1] = findAndAssignAttacks(attack_2_ID);
moves[2] = findAndAssignAttacks(attack_3_ID);
moves[3] = findAndAssignAttacks(attack_4_ID);
//std::cout << "Attack  id: " << attack_4_ID << std::endl;

}

void Pokemon::printPokemonInfo()
{
    recalculateStats();
    std::cout <<"======="<<name<<"'s Information =======" << std:: endl;
    std::cout << "Name: " << name << std::endl;
    std::cout << "Health: " << hp << std::endl;
    std::cout << "Attack: " << attack << std::endl;
    std::cout << "Defense: " << defense << std::endl;
    std::cout << "Special Attack: " << specialAttack << std::endl;
    std::cout << "Special Defense: " << specialDefense << std::endl;
    std::cout << "Speed: " << speed << std::endl;
    std::cout << "Level: " << level << std::endl;
    std::cout << "Current Experience: " << currentEXP << std::endl;
    std::cout << "Required Experience: " << requiredEXP << std::endl;
    std::cout << "Attack 1: " << moves[0].name << std::endl;
    std::cout << "Attack 2: " << moves[1].name << std::endl;
    std::cout << "Attack 3: " << moves[2].name << std::endl;
    std::cout << "Attack 4: " << moves[3].name << std::endl;
}
void Pokemon::recalculateStats()
{


    std::string hpbasestat="hp";
    //std::cout<< "Health stat: 5555555555555555555555555555555555" << getPokemonBaseStat(hpbasestat, name) << std::endl;
    hp = ((2*getPokemonBaseStat("hp", name)+100)*level+10)/100;
    attack = ((2*getPokemonBaseStat("attack", name)+100)*level+10)/100;
    defense = ((2*getPokemonBaseStat("defense", name)+100)*level+10)/100;
    specialAttack = ((2*getPokemonBaseStat("specialAttack", name)+100)*level+10)/100;
    specialDefense = ((2*getPokemonBaseStat("specialDefense", name)+100)*level+10)/100;
    speed = ((2*getPokemonBaseStat("speed", name)+100)*level+10)/100;
}
unsigned int Pokemon::getPokemonBaseStat(std::string stat, std::string pokemonName)
{

    bool found = false;

    std::string lineOfDesiredPokemon= findPokemonInFile(pokemonName);
    std::vector<std::string> pokemonInfo;
    std::string temporaryString;
    for(unsigned int x = 0; x < lineOfDesiredPokemon.length(); x++)
        // EXAMPLE: Bulbasaur att:10 def:10 spa:10 spd:10 spe:10 at1:1 at2:2 at3:3 at4:4 lvl:1 cxp:0 nxp:1000
    {
        if(lineOfDesiredPokemon[x] != ':') //Identifies each attribute via the space
        {
            temporaryString+=lineOfDesiredPokemon[x];
           // std::cout << "temporary string: " << temporaryString << std::endl;
        }
        else
        {
            pokemonInfo.push_back(temporaryString);
            //std::cout << " final temporary string: " << temporaryString << std::endl;
            temporaryString="";
        }
    }
     if(stat == "hp")
     {
        // std::cout << "converted HP base " << atoi(pokemonInfo[1].c_str());
         return atoi(pokemonInfo[1].c_str());
     }
     if(stat == "attack")
     {
         return atoi(pokemonInfo[2].c_str());
     }
     if(stat == "defense")
     {
         return atoi(pokemonInfo[3].c_str());
     }
     if(stat == "specialAttack")
     {
         return atoi(pokemonInfo[4].c_str());
     }
     if(stat == "specialDefense")
     {
         return atoi(pokemonInfo[5].c_str());
     }
     if(stat == "speed")
     {
         return atoi(pokemonInfo[6].c_str());
     }



}
std::string Pokemon::findPokemonInFile(std::string pokemonName)
{
    std::ifstream readPokemonFile("Data/Pokemon/All Pokemon Stats.txt.txt");
    std::string temporary;
    std::string lineOfDesiredPokemon = "";
        while (std::getline(readPokemonFile,temporary))
        {
                           std::string nameofprocessedline="";
            for (unsigned int x = 0; x < pokemonName.size(); x++)
            {
                nameofprocessedline+=temporary[x];

            }
            if(nameofprocessedline==pokemonName)
            {
                lineOfDesiredPokemon=temporary;
                break;
            }

        }
     return lineOfDesiredPokemon;
}
Attack Pokemon::findAndAssignAttacks(unsigned int desiredAttackID)
{
    //std::cout << "allmoves size :" << allmoves.size() << std::endl;
    try
    {
    bool foundAttack = false;
  for (unsigned int x = 0; x < allmoves.size(); x++)
  {
      if (allmoves[x].ID == desiredAttackID)
      {
          foundAttack = true;
          return allmoves[x];
      }
  }
  if(foundAttack==false)
  {
      throw "attack not found.";
  }
    }
    catch(...)
    {
        for (unsigned int c = 0; c < allmoves.size();c++)
        {
            std::cout << allmoves[c].name << std::endl;
        }
        std::cout << "an attack with that ID wasn't found. Check the allmoves vector in main.cpp and the allmoves.txt file and possibly the function that assigns the move ids to each pokemon." << std::endl;
    }
}
